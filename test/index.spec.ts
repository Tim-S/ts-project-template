import { expect} from "chai";
import "mocha";
import { main } from "../src/index";

describe("main()", () => {
    it("should return 'Hello World!'", () => {
        var result = main();
        expect(result).to.equal('Hello World!');
    });
});
